<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::create([
            'name' => 'bin',
            'email' => 'bin@gmail.com',
            'password' => Hash::make('admin1')
        ]);
        $user = User::create([
            'name' => 'user',
            'email' => 'user@gmail.com',
            'password' => Hash::make('admin2')
        ]);

        $admin->assignRole('admin');
        $role = Role::find(1)->givePermissionTo(Permission::all());

        $user->assignRole('user')->givePermissionTo('edit user', 'view user');
    }
}
