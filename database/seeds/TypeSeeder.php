<?php

use Illuminate\Database\Seeder;
use App\Type;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Type::create([
        	'name' => 'PNS',
        	'slug' => 'pns'
        ]);
        Type::create([
        	'name' => 'Honored',
        	'slug' => 'honor'
        ]);
    }
}
