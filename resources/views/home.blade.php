@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

@can('view user')
<!-- Teacher Data -->
           <div class="text-center" style="letter-spacing: 14px;padding: .5cm 0;"><h1>Teach</h1></div>

            <table class="table table-hover">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Name</th>
                  <th scope="col">Stats</th>
                  <th scope="col">Handle</th>
                </tr>
              </thead>
    <?php $no = 1; ?>
      @forelse ($Teach as $teach_home)
              <tbody>
                    <tr>
                      <th scope="row">{{ $no }}</th>
                      <td>{{ $teach_home->name }}</td>
                      <td>{{ $teach_home->type->name }}</td>
                      <td>
                        @role('admin')
                          <a href="{{ route('teach.edit', $teach_home->id) }}">edit</a> 
                          | 
                        @endrole
                        @can('view user')
                          <a style="color: red;" href="{{ route('teach.delete', $teach_home->id) }}">x</a>
                        @endcan
                      </td>
                    </tr>
              </tbody>
    <?php $no++; ?>
        @empty
        <!-- EMPTY -->    
              <tbody>
                <tr>
                  
                    <th scope="row"><a href="{{ route('teach.create') }}" id=""> kosong !</a></th>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
              </tbody>
        <!-- END EMPTY -->
            </table>
    @endforelse
<!-- End Teacher Data -->
        </div>
@endcan
    </div>
</div>
@endsection
