@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
			<div class="text-center" style="letter-spacing: 14px;padding: 1cm 0;"><h1>Teach Edit</h1></div>
        </div>
		<div class="col-md-5">
			<form action="{{ route('teach.update', $Teach->id) }}" method="post">
				{{ csrf_field() }}
				{{ method_field('PATCH') }}
				<div class="form-group">
					<input type="text" class="form-control" name="name" value="{{ $Teach->name }}">
				</div>

				<div class="form-group">
					<input type="email" class="form-control" name="email" value="{{ $Teach->email }}">
				</div>

				<div class="form-group ">
					<input type="date" name="born" class="col-md-4" value="{{ $Teach->born }}">

						<select name="type_id" class=" col-md-3 float-right">
						@foreach ($Type as $type_create)
							<option 
								value="{{ $type_create->id }}"

								@if($type_create->id === $Teach->type_id)
									selected
								@endif
								>
								{{ $type_create->name }}
							</option>	
						@endforeach
						</select>
				</div>

				<div class="form-group">
					<button class="btn float-right">Save</button>
				</div>
			</form>
		</div>
    </div>
</div>
@endsection
