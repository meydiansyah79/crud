<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserController extends Controller
{
    public function create()
    {
    	$crt = User::create([
    		'name' => request('name'),
    		'email' => request('email'),
    		'password' => Hash::make(request('password'))
    	]);

    	$crt->givePermissionTo('view user');

    	return redirect()->route('home');
    }

    public function index()
    {
    	return view('user.create');
    }
}
