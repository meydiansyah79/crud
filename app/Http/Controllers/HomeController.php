<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Teach;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $Teach = Teach::all();
        return view('home', compact('Teach'));
    }
}
