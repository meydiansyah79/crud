<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Type;
use App\Teach;

class TeachController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Type = Type::all();
        return view('teach.create', compact('Type'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       Teach::create([
            'name' => request('name'),
            'email' => request('email'),
            'born' => request('born'),
            'type_id' => request('type_id'),
       ]);

       return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Teach $Teach)
    {
        $Type = Type::all();
        return view('teach.edit', compact('Teach', 'Type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Teach $Teach)
    {
        $Teach->update([
            'name' => request('name'),
            'email' => request('email'),
            'born' => request('born'),
            'type_id' => request('type_id'),
        ]);
        return redirect()->route('home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Teach $Teach)
    {
        $Teach->delete();
        return redirect()->route('home');
    }
}
