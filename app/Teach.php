<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teach extends Model
{
    protected $fillable = ['name', 'type_id', 'born', 'email',];

	public function Type()
	{
		return $this->belongsTo(Type::class);
	}
}
