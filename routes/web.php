<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
	Route::get('/home', 'HomeController@index')->name('home');
	
	Route::get('/teach/create', 'TeachController@index')->name('teach.create');
	Route::post('/teach/create', 'TeachController@store')->name('teach.store');

	Route::get('/teach/{Teach}/edit', 'TeachController@edit')->name('teach.edit');
	Route::patch('/teach/{Teach}/edit', 'TeachController@update')->name('teach.update');

	Route::get('/teach/{Teach}/delete', 'TeachController@destroy')->name('teach.delete');

	//User Controller
	Route::get('/user', 'UserController@index')->name('user.create');
	Route::post('/user/create', 'UserController@create')->name('user.add');
});